<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="style.css">
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.3.1/css/all.min.css">
	<title>Daftar Akun</title>
</head>
<body>

	<!-- Inline CSS -->
	<style type="text/css">
		.login100-form-title {
			font-size: 25px;
			color: #403866;
			line-height: 1.2;
			text-transform: uppercase;
			text-align: center;
			width: 100%;
			display: block;
			margin-top: 0;
			padding-top: 20px;
		}
		.login100-form-title i {
			font-size: 50px;
			color: #fff;
			background-color: #2691d9;
			border: 1.5px solid #000000;
			width: 100px;
			border-radius: 100%;
			height: 100px;
			line-height: 90px;
			transition: 0.5s case-in-out;
		}
		.login100-form-title i:hover {
			background-color: #fff;
			color: #2691d9;
			border-color: #000000;
		}
		.center h1 {
			padding: 0;
			margin: 0;
		}
		.alert {
			border: 0;
			margin: 0;
		}
		form .txt_field {
			margin: 20px 0 0 0;
		}
		#confirm_password {
			margin-bottom: 20px;
		}
		input[type="submit"] {
			margin: 0;
		}
		.home {
			margin: 10px 0 15px 0;
			text-align: center;
			font-size: 16px;
			color: #666666;
		}
		.home a {
			color: #2691d9;
			text-decoration: none;
			}
		.home a {
			text-decoration: underline;
		}
	</style>

	<!-- Title -->
	<div class="center">
		<span class="login100-form-title">
			<i class="fas fa-user-plus"></i>
		</span>
		<h1>Tambah User</h1>
		<?php 

			if (isset($_GET["pesan"])){
			$pesan = $_GET["pesan"];
			}
			else {
			$pesan = "Silahkan Isi Data";
			}

		?>
		
		<!-- Form -->
		<div class="alert alert-warning alert-dismissible fade show" role="alert">
  			<?php echo $pesan;?>
  			<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
		</div>
		
		<form method="POST" action="proses_register.php">
			<div class="txt_field">
				<input type="text" name="name" required>
				<label>Nama</label>
				<span></span>
			</div>
			<div class="txt_field">
				<input type="text" name="username" required>
				<label>Username</label>
				<span></span>
			</div>
			<div class="txt_field">
				<input type="email" name="email" required>
				<label>Email</label>
				<span></span>
			</div>
			<div class="txt_field">
				<input type="password" name="password" required>
				<label>Password</label>
				<span></span>
			</div>
			<div class="txt_field" id="confirm_password">
				<input type="password" name="confirm_password" required>
				<label>Konfirmasi Password</label>
				<span></span>
			</div>
			<input type="submit" value="Tambah User">
			<div class="home">
				Kembali<a href="index.php"> ke Home</a>
			</div>
		</form>
	</div>

	<!-- Bootstrap JS -->
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
</body>
</html>