<?php
session_start();

if (isset($_SESSION["login"])) {
	header("Location: home.php");

	exit();
}

 ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="style.css">
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.3.1/css/all.min.css">
	<title>Login Blog</title>
</head>
<body>
	<style type="text/css">
		.login100-form-title {
			font-size: 25px;
			color: #403866;
			line-height: 1.2;
			text-transform: uppercase;
			text-align: center;
			width: 100%;
			display: block;
			margin-top: 10px;
			padding-top: 20px;
		}
		.login100-form-title i {
			font-size: 50px;
			color: #fff;
			background-color: #2691d9;
			border: 1.5px solid #000000;
			width: 100px;
			border-radius: 100%;
			height: 100px;
			line-height: 90px;
			transition: 0.5s case-in-out;
		}
		.login100-form-title i:hover {
			background-color: #fff;
			color: #2691d9;
			border-color: #000000;
		}
		.center h1 {
			padding: 10px 0 20px 0;
		}
		.alert {
			margin: 0;
		}
		form .txt_field {
			margin: 20px 0 0 0;
		}
		input[type="submit"] {
				margin: 20px 0 0 0;
		}
		.daftar {
			margin: 10px 0 15px 0;
		}
	</style>
	<div class="center">
		<span class="login100-form-title">
			<i class="fas fa-user"></i> <!-- untuk login -->
			<!-- <i class="fas fa-user-plus"> -> untuk register </i>-->
		</span>
		<h1>Login</h1>
		<?php 

			if (isset($_GET["pesan"])){
			$pesan = $_GET["pesan"];
			}
			else {
			$pesan = "Silahkan Login";
			}
			
		?>
		<div class="alert alert-warning alert-dismissible fade show" role="alert">
  			<?php echo $pesan;?>
  			<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
		</div>
		<form method="POST" action="proses_login.php">
			<div class="txt_field">
				<input type="text" name="username" required>
				<label>Username</label>
				<span></span>
			</div>
			<div class="txt_field">
				<input type="password" name="password" id="password" required>
				<label>Password</label>
				<span></span>
			</div>
			<input type="submit" value="Login">
			<div class="daftar">
				Belum punya akun?<a href="register.php"> Daftar</a>
			</div>
		</form>
	</div>

	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
</body>
</html>