<?php 

include 'koneksi.php';

$id = $_GET["id"];

$sql = "SELECT * FROM users WHERE id='$id'";
$result = $koneksi -> query($sql);
$hasil = mysqli_fetch_assoc($result);

 ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.3.1/css/all.min.css">
	<link rel="stylesheet" type="text/css" href="style.css">
	<title>Ubah Data User</title>
</head>
<body>

	<!-- Inline CSS -->
	<style type="text/css">
		.login100-form-title {
			font-size: 25px;
			color: #403866;
			line-height: 1.2;
			text-transform: uppercase;
			text-align: center;
			width: 100%;
			padding-top: 10px;
			display: block;
			margin-top: 0;
		}
		.login100-form-title i {
			font-size: 50px;
			color: #fff;
			background-color: #2691d9;
			border: 1.5px solid #000000;
			width: 100px;
			padding-bottom: 20px;
			border-radius: 100%;
			height: 100px;
			line-height: 90px;
			transition: 0.5s case-in-out;
		}
		.login100-form-title i:hover {
			background-color: #fff;
			color: #2691d9;
			border-color: #000000;
		}
		form .txt_field {
			margin: 20px 0 0 0;
		}
		.container{
			padding: 12px 12px 12px 12px;
		}
		#header{
			margin: 10px 0 10px 0;
		}
		#email {
			margin-bottom: 20px;
		}
		input[type="submit"] {
			margin: 0;
		}
		.footer {
		   position: fixed;
		   left: 0;
		   bottom: 0;
		   width: 100%;
		   background-color: transparent;
		   color: #adadad;
		   text-align: left;
		   padding: 15px 0px 0px 10px;
		}
	</style>

	<!-- Navbar -->
	<nav class="navbar navbar-expand-lg navbar navbar-dark bg-dark">
		<div class="container-fluid">
    		<div class="navbar-brand">BLOG</div>
		    	<button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
		     		<span class="navbar-toggler-icon"></span>
		    	</button>
    			<div class="collapse navbar-collapse" id="navbarSupportedContent">
     				<ul class="navbar-nav me-auto mb-2 mb-lg-0">
        				<li class="nav-item">
         					<a class="nav-link active" aria-current="page" href="home.php">Home</a>
        				</li>
        				<li class="nav-item dropdown">
          					<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
            					More
         					</a>
        					<ul class="dropdown-menu" aria-labelledby="navbarDropdown">
            					<li><a class="dropdown-item" href="#">User</a></li>
            					<li><hr class="dropdown-divider"></li>
            					<li><a class="dropdown-item" href="#">About</a></li>
        					</ul>
        				</li>
      				</ul>
      			</div>
  				<div class="d-flex">
      	  			<a class="btn btn-primary" href="logout.php" role="button">Logout</a>
      			</div>
    		</div>
 		</div>
	</nav>

	<!-- Header -->
	<div>
		
	</div>

	<!-- Form -->
	<div class="center">
		<div class="container">
			<span class="login100-form-title">
				<i class="fas fa-user-edit"></i>
			</span>
			<div class="d-flex justify-content-between align-items-center mb-3" id="header">
				<h5 class="card-title">Ubah Data User</h5>
			  	<a class="btn btn-primary" href="home.php" role="button">Kembali</a>
			</div>
			<form method="POST" action="proses_ubah.php">
				<input type="hidden" name="id" value="<?= $id ?>">
				<div class="txt_field">
					<input type="text" name="name" value="<?= $hasil["name"]  ?>" required>
					<label>Nama</label>
					<span></span>
				</div>
				<div class="txt_field">
					<input type="text" name="username" value="<?= $hasil["username"]  ?>" required>
					<label>Username</label>
					<span></span>
				</div>
				<div class="txt_field" id="email">
					<input type="email" name="email" value="<?= $hasil["email"]  ?>" required>
					<label>Email</label>
					<span></span>
				</div>
				<!-- <div class="txt_field">
					<input type="password" name="password" required>
					<label>Password</label>
					<span></span>
				</div>
				<div class="txt_field" id="confirm_password">
					<input type="password" name="confirm_password" required>
					<label>Konfirmasi Password</label>
					<span></span>
				</div> -->
				<input type="submit" value="Ubah Data">
			</form>
		</div>
	</div>

	<!-- Footer -->
	<div class="footer">
		<footer class="footer mt-auto py-3 bg-dark">
			<div class="container">
				<span class="text-muted">© by achmd.dhani</span>
			</div>
		</footer>
	</div>


	<!-- Bootstrap Min JS -->
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
</body>
</html>