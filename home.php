<?php
session_start();

include 'koneksi.php';

if(!isset($_SESSION["login"])) {
	header("Location: index.php");

	exit();
}

if (isset($_GET["pesan"])) {
	if ($_GET["pesan"] === "berhasil_hapus") {
		$pesan = "Berhasil Menghapus User";
		$warna = "success";
	}
	if ($_GET["pesan"] === "gagal_hapus") {
		$pesan = "Gagal Menghapus User";
		$warna = "danger";
	}
	if ($_GET["pesan"] === "berhasil_ubah") {
		$pesan = "Berhasil Mengubah Data User";
		$warna = "success";
	}
	if ($_GET["pesan"] === "gagal_ubah") {
		$pesan = "Gagal Mengubah Data User";
		$warna = "danger";
	}
}

$username = $_SESSION['username'];

$sql = "SELECT * FROM users";
$result = mysqli_query($koneksi, $sql);
$hasil = mysqli_fetch_all($result, MYSQLI_ASSOC);

 ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Home</title>
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.3.1/css/all.min.css">
	<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>

	<!-- Inline CSS -->
	<style type="text/css">
		#gmail-img {
			width: 40px;
			height: 25px;
			padding-left: 5px;
		}

		#gmail {
			padding: 0px;
		}

		#ig-img {
			width: 35px;
			height: 30px;
			padding-left: 5px;
		}

		#ig {
			padding: 0px;
		}

		#wa-img {
			width: 35px;
			height: 30px;
			padding-left: 5px;
		}

		#wa {
			padding: 0px;
		}
		.title {
			text-align: center;
			margin: 30px 0 30px 0;
			font-family: montserrat;
		}
		.login100-form-title {
			font-size: 25px;
			color: #403866;
			line-height: 1.2;
			text-transform: uppercase;
			text-align: center;
			width: 100%;
			padding-bottom: 20px;
			display: block;
			margin-top: 0;
		}
		.login100-form-title i {
			font-size: 50px;
			color: #fff;
			background-color: #2691d9;
			border: 1.5px solid #000000;
			width: 100px;
			padding-bottom: 20px;
			border-radius: 100%;
			height: 100px;
			line-height: 90px;
			transition: 0.5s case-in-out;
		}
		.login100-form-title i:hover {
			background-color: #fff;
			color: #2691d9;
			border-color: #000000;
		}
		.my-custom-scrollbar {
		position: relative;
		height: 200px;
		overflow: auto;
		}
		.table-wrapper-scroll-y {
		display: block;
		}
		.footer {
		   position: fixed;
		   left: 0;
		   bottom: 0;
		   width: 100%;
		   background-color: transparent;
		   color: #adadad;
		   text-align: left;
		   padding: 15px 0px 0px 10px;
		}
	</style>

	<!-- Navbar -->
	<nav class="navbar navbar-expand-lg navbar navbar-dark bg-dark">
		<div class="container-fluid">
    		<div class="navbar-brand">BLOG</div>
		    	<button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
		     		<span class="navbar-toggler-icon"></span>
		    	</button>
    			<div class="collapse navbar-collapse" id="navbarSupportedContent">
     				<ul class="navbar-nav me-auto mb-2 mb-lg-0">
        				<li class="nav-item">
         					<a class="nav-link active" aria-current="page" href="home.php">Home</a>
        				</li>
        				<div class="nav-item dropdown">
							<a class="nav-link dropdown-toggle" href="#" role="button" id="navbarDropdown" data-bs-toggle="dropdown" aria-expanded="false">
								Contact
						  	</a>

						  	<ul class="dropdown-menu" aria-labelledby="dropdownMenuLink">
							    <li><a class="dropdown-item" href="mailto:achmadhani08@gmail.com" target="_blank" id="gmail"><img src="gmail.png" title="Gmail" id="gmail-img"> Gmail</a></li>
							    <li><hr class="dropdown-divider"></li>
							    <li><a class="dropdown-item" href="https://www.instagram.com/achmd.dhani/" target="_blank" id="ig"><img src="ig.png" title="Instagram" id="ig-img"> Instagram</a></li>
							    <li><hr class="dropdown-divider"></li>
							    <li><a class="dropdown-item" href="https://wa.me/6285156785548" target="_blank" id="wa"><img src="wa.jpg" title="WhatsApp" id="wa-img"> WhatsApp</a></li>
						  	</ul>
						</div>
      				</ul>
  				<div class="d-flex">
      	  			<a class="btn btn-primary" href="logout.php" role="button">Logout</a>
      			</div>
    		</div>
 		</div>
	</nav>

	<!-- Title -->
	<h1 class="title">Hola Blogger!</h1>
	<div>
		<span class="login100-form-title">
			<i class="fas fa-users"></i>
		</span>
	</div>
	<div class="container">
		<div class="d-flex justify-content-around align-items-center mb-3">
			<h3>Data Users</h3>
		
			<?php
			if (isset($pesan)) {
			 	?>
			 	<div class="alert alert-<?= $warna; ?> alert-dismissible fade show" role="alert">
  					<?= $pesan; ?>
  					<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
				</div>
				<?php
			} 
			?>

			<a class="btn btn-primary h3" href="tambah_user.php">Tambah User</a>
		</div>	
		
		<!-- Data Users -->
		<div class="table-wrapper-scroll-y my-custom-scrollbar">
			<table class="table table-light table-hover text-center">
				<thead>
			    <tr>
			     	<th scope="col">No.</th>
			     	<th scope="col">Name</th>
			     	<th scope="col">Username</th>
			      	<th scope="col">Email</th>
			      	<th scope="col">Avatar</th>
			     	<th scope="col">Action</th>
			    </tr>
				</thead>
				<tbody>
					<?php 
					foreach ($hasil as $key => $user_data) {
					 ?>
				    <tr>
				    	<td> 
				     		<?= $key + 1 ?>
				     	</td>
				     	<td> 
				     		<?= $user_data["name"] ?>
				     	</td>
				     	<td> 
				     		<?= $user_data["username"] ?>
				     	</td>
				     	<td>
				     		<?= $user_data["email"] ?>
				     	</td>
				     	<td>
				     		<?= $user_data["avatar"] ?>
				     	</td>
				     	<td>
				     		<a class="btn btn-success" href="form_ubah.php?id=<?= $user_data['id'] ?>"><i class="fas fa-pen"> Ubah</i></a>
				     		<a class="btn btn-danger" href="proses_hapus.php?id=<?= $user_data['id'] ?>"><i class="fas fa-trash"> Hapus</i></a>
				     	</td>
				   	</tr>
				   	<?php 
				   	}
				   	 ?>
				</tbody>
			</table>
		</div>
	</div>

	<!-- Footer -->
	<div class="footer">
		<footer class="footer mt-auto py-3 bg-dark">
			<div class="container">
				<span class="text-muted">© by achmd.dhani</span>
			</div>
		</footer>
	</div>
	

	<!-- Bootstrap Min JS -->
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
</body>
</html>